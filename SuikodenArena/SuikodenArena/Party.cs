﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuikodenArena
{
    public class Party
    {
        /// <summary>
        /// Use this to give the party some kind of identity
        /// </summary>
        public string Name { get; set; }

        // DO NOT CHANGE THESE 2 LINES
        private List<Unit> units = new List<Unit>();
        public IEnumerable<Unit> Units { get { return units; } }

        /// <summary>
        /// If there's no more units left (units list count is 0), return true. False, otherwise
        /// </summary>
        public bool Wiped
        {
            get
            {
                if (units.Count <= 0) { return true; }
                else { return false; }
            }
        }

        /// <summary>
        /// Add the unit to the 'units' list
        /// </summary>
        /// <param name="unit"></param>
        public void AddUnit(Unit unit)
        {
            units.Add(unit);
        }

        /// <summary>
        /// Get the next unit in queue. This unit is the index 0 of the 'units' list. 
        /// Remove the returned unit from the list.
        /// </summary>
        /// <returns>If the list is empty, return null. Returns the unit at index 0 if the list has at least one unit.</returns>
        public Unit NextUnit()
        {
            if (units.Count <= 0) { return null; }
            else
            {
                Unit temp = new Unit(); //my attempt at removing an item from a list BEFORE returning it
                temp = units[0];
                units.Remove(units[0]);
                return temp;
            }
        }

        /// <summary>
        /// Print team name and all the units in the party (Console.WriteLine)
        /// </summary>
        public void DisplayParty()
        {
            Console.WriteLine(Name);

            for (int i = 0; i < 30; i++) { Console.Write("="); }    // line printer
            Console.WriteLine();

            for (int i = 0; i < units.Count; i++)   //prints the units
            {
                Console.WriteLine(units[i].Name + " HP: " + units[i].CurrentHp + "/" + units[i].MaxHp + "   POW: " + units[i].Power + " DEF:" + units[i].Defense);
            }
            Console.WriteLine();
        }
    } // end class Party
} // end namespace SuidokenDuel
