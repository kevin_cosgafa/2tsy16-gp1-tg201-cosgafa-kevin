﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuikodenArena
{
    class Program
    {
        // 1. Initialize the 2 parties

        // 2. Display the 2 parties (use Party.DisplayParty)

        // 3. Loop until at least one party is wiped

        // 3.1 Get the combatants (use Party.NextUnit())

        // 3.2 Loop until at least one unit is dead
        // 3.2.1 Display the units in battle (use Unit.DisplayStats())
        // 3.2.2 Set action for both units (use Unit.SetUserAction and Unit.SetRandomAction)
        // 3.2.3 Evaluate actions of the 2 units

        // 4. Evaluate the winner (may result in a win, lose, or draw)

        // NOTE: Take advantage of the Player and Unit object. Call the object's function whenever applicable.
        // NOTE: Please use functions to implement the listed items

        //borrowed for random stats of the units
        private static readonly Random random = new Random();

        //forgive me for i do not know the c# alternative of the c++ system("PAUSE")
        static void Pause()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        //places units inside the parties (for item 1)
        static void Item1(Party teamA, Party teamB)
        {
            teamA.Name = "Kings";
            teamB.Name = "Queens";

            //unit randomizer
            for (int i = 0; i < 5; i++)
            {
                //random stats
                Unit newGuy = new Unit();
                newGuy.MaxHp = random.Next(31, 38);
                newGuy.CurrentHp = newGuy.MaxHp;
                newGuy.Power = random.Next(10, 18);
                newGuy.Defense = random.Next(5, 6);

                Unit badGuy = new Unit();
                badGuy.MaxHp = random.Next(31, 38);
                badGuy.CurrentHp = badGuy.MaxHp;
                badGuy.Power = random.Next(10, 18);
                badGuy.Defense = random.Next(5, 6);

                //names
                switch (i)
                {
                    case 0:
                        newGuy.Name = "Max";
                        badGuy.Name = "Kev";
                        break;
                    case 1:
                        newGuy.Name = "Mam";
                        badGuy.Name = "Sar";
                        break;
                    case 2:
                        newGuy.Name = "Cli";
                        badGuy.Name = "Ari";
                        break;
                    case 3:
                        newGuy.Name = "Jan";
                        badGuy.Name = "Bry";
                        break;
                    case 4:
                        newGuy.Name = "Mic";
                        badGuy.Name = "Niw";
                        break;
                }

                //add to party
                teamA.AddUnit(newGuy);
                teamB.AddUnit(badGuy);
            }//end for (int i = 0; i < 5; i++)
        }//end item1(Party teamA, Party teamB)

        //adds extra writeline and a system pause (for item 2)
        static void Item2(Party teamA, Party teamB)
        {
            teamA.DisplayParty();
            Console.WriteLine();
            teamB.DisplayParty();
            Pause();
        }

        //the whole combat scenario is in this function
        static void Battle(Party teamA, Party teamB)
        {
            Unit currentPlayer = new Unit(), currentEnemy = new Unit(); //current player and enemy
            currentPlayer.CurrentHp = 0;
            currentEnemy.CurrentHp = 0;

            while ((!teamA.Wiped || currentPlayer.CurrentHp > 0) && (!teamB.Wiped || currentEnemy.CurrentHp > 0))
            {
                //item 3.1
                if (currentPlayer.CurrentHp <= 0) { currentPlayer = teamA.NextUnit(); }
                if (currentEnemy.CurrentHp <= 0) { currentEnemy = teamB.NextUnit(); }

                while (currentPlayer.Alive && currentEnemy.Alive) //item 3.2
                {
                    //item 3.2.1
                    Console.Clear();
                    currentPlayer.DisplayStats();
                    Console.WriteLine("VS");
                    currentEnemy.DisplayStats();
                    Console.WriteLine();

                    //item 3.2.2
                    currentPlayer.SetUserAction();
                    currentEnemy.SetRandomAction();

                    //item 3.2.3
                    currentPlayer.EvaluateAction(currentEnemy);
                    Console.WriteLine();
                    Pause();

                }// end while (currentPlayer.Alive && currentEnemy.Alive)

                //console output for characters who die
                Console.Clear();
                if (!currentPlayer.Alive) { Console.WriteLine(currentPlayer.Name + " dies."); }
                if (!currentEnemy.Alive) { Console.WriteLine(currentEnemy.Name + " dies."); }
                Console.WriteLine();
                Pause();
            }// end while (!teamA.Wiped && !teamB.Wiped)

            //item 4
            Console.Clear();
            if (!teamA.Wiped || currentPlayer.CurrentHp > 0) { Console.WriteLine(teamA.Name + " WINS!"); }
            else if (!teamB.Wiped || currentEnemy.CurrentHp > 0) { Console.WriteLine(teamB.Name + " WINS!"); }
            else { Console.WriteLine("BOTH TEAMS ARE DEAD. DRAW"); }
        }

        static void Main(string[] args)
        {
            //item 1
            Party pTeam = new Party(), eTeam = new Party();
            Item1(pTeam, eTeam);

            //item 2
            Item2(pTeam, eTeam);

            //items 3-4 
            Battle(pTeam, eTeam);
        } // end main(string[] args)
    } // end class Program
} // end namespace SuidokenDUel
