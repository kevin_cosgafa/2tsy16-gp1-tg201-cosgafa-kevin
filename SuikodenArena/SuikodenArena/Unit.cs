﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuikodenArena
{
    /// <summary>
    /// Possible actions a unit can execute. Here's an example on how to use this
    /// Some examples:
    /// Ex#1 Action action = ActionType.Attack
    /// Ex#2 if (action == ActionType.Attack)
    /// Ex#3 return ActionType.Attack
    /// </summary>
    public enum ActionType
    {
        None,
        Attack,
        Defend,
        WildAttack
    }
    public class Unit
    {
        /// <summary>
        /// Use this object to randomize value. This will prevent getting the same values.
        /// To use, call this function: random.Next(int maxvalue) or random.Next(int minValue, int maxValue);
        /// </summary>
        private static readonly Random random = new Random();

        public string Name { get; set; }
        public int Power { get; set; }
        public int Defense { get; set; }
        public int MaxHp { get; set; }

        /// <summary>
        /// Return true if CurrentHp is greater than 0. False, otherwise.
        /// </summary>
        public bool Alive
        {
            get
            {
                if (CurrentHp > 0) { return true; }
                else { return false; }
            }
        }

        /// <summary>
        /// The variable being set by SetUserAction and SetRandomAction (please refer to these functions). DON'T MODIFY THIS LINE
        /// </summary>
        public ActionType Action { get; private set; }

        /// <summary>
        /// Write an appropriate algorithm for clamping the HP between 0 and MaxHp
        /// For getter, simply return 'currentHp'
        /// For setter, update 'currentHp' value. Make sure that 'currentHp' is set to a value between 0 and MaxHp
        /// </summary>
        private int currentHp;
        public int CurrentHp
        {
            // Don't change this
            get { return currentHp; }

            // Implement this
            set
            {
                currentHp = value;
                if (CurrentHp > MaxHp) { currentHp = MaxHp; }
                if (CurrentHp < 0) { currentHp = 0; }
            }
        }

        /// <summary>
        /// Randomize damage based on Power.
        /// Minimum damage = Your Power
        /// Maximum damage = 120% of your power (eg. if Power = 100, then max damage = 120)
        /// </summary>
        /// <returns></returns>
        public int RandomizeDamage()
        {
            return random.Next(Power, Power + (Power / 5));
        }

        /// <summary>
        /// Evaluate this unit's action and the target's action. Based on the action, deal damage with appropriate damage modifier (if applicable). 
        /// This function only tries to damage the target from the perspective of this unit. Target's damage will be evaluated on the target's side.
        /// Refer to the matchup table in the spec for damage modifiers.
        /// TIP: Read this.Action and target.Action and compare these two.
        /// </summary>
        /// <param name="target"></param>
        public void EvaluateAction(Unit target)
        {
            //prints player action
            if (this.Action == ActionType.Attack) { Console.WriteLine(this.Name + " attacks..."); }
            if (this.Action == ActionType.Defend) { Console.WriteLine(this.Name + " defends..."); }
            if (this.Action == ActionType.WildAttack) { Console.WriteLine(this.Name + " is preparing for a special attack..."); }

            //prints ai action
            if (target.Action == ActionType.Attack) { Console.WriteLine(target.Name + " attacks..."); }
            if (target.Action == ActionType.Defend) { Console.WriteLine(target.Name + " defends..."); }
            if (target.Action == ActionType.WildAttack) { Console.WriteLine(target.Name + " is preparing for a special attack..."); }

            Console.WriteLine();

            //Matchups
            int thisDamage = 0, targetDamage = 0;
            if (this.Action == ActionType.Attack) //this actions with if target defends case
            {
                thisDamage = this.RandomizeDamage();
                if (target.Action == ActionType.Defend) { thisDamage /= 2; }
            }
            else if (this.Action == ActionType.WildAttack)
            {
                thisDamage = this.RandomizeDamage() * 2;
                if (target.Action == ActionType.Defend)
                {
                    thisDamage = 0;
                    targetDamage = target.RandomizeDamage() * 2;
                }
            }

            if (target.Action == ActionType.Attack) //target actions with if this defends case
            {
                targetDamage = target.RandomizeDamage();
                if (this.Action == ActionType.Defend) { targetDamage /= 2; }
            }
            else if (target.Action == ActionType.WildAttack)
            {
                targetDamage = target.RandomizeDamage() * 2;
                if (this.Action == ActionType.Defend)
                {
                    targetDamage = 0;
                    thisDamage = this.RandomizeDamage() * 2;
                }
            }

            //Evaluation
            target.CurrentHp -= thisDamage;
            this.CurrentHp -= targetDamage;
            if (thisDamage > 0)
            {
                Console.Write(this.Name + " dealt " + thisDamage + " damage to " + target.Name);
                if (this.Action == ActionType.Defend) { Console.Write(" C-C-COUNTER!"); }
                else if (this.Action == ActionType.WildAttack) { Console.Write(" CRITICAL!"); }
                Console.WriteLine();
            }
            if (targetDamage > 0)
            {
                Console.Write(target.Name + " dealt " + targetDamage + " damage to " + this.Name);
                if (target.Action == ActionType.Defend) { Console.Write(" C-C-COUNTER!"); }
                else if (target.Action == ActionType.WildAttack) { Console.Write(" CRITICAL!"); }
                Console.WriteLine();
            }
        } //end EvaluateAction(Unit target)

        /// <summary>
        /// Call this to set action based from user input.
        /// Ask the user for input and return an enum value.
        /// After determining the action, set the Action variable based on evaluated value.
        /// TIP: Ask the player to input a number. Where a number is tied to one action
        ///     1 = ActionType.Attack
        ///     2 = ActionType.Defend
        ///     3 = ActionType.WildAttack
        /// </summary>
        public void SetUserAction()
        {
            Console.WriteLine(" Choose an action:");
            Console.WriteLine(" [1] Attack");
            Console.WriteLine(" [2] Defend");
            Console.WriteLine(" [3] Wild Attack");

            bool clear = false; //for false inputs
            while (!clear)
            {
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        Action = ActionType.Attack;
                        clear = true;
                        break;
                    case "2":
                        Action = ActionType.Defend;
                        clear = true;
                        break;
                    case "3":
                        Action = ActionType.WildAttack;
                        clear = true;
                        break;
                    default: //fail safe
                        clear = false;
                        break;
                }
            }
        }

        /// <summary>
        /// Call this to get action from AI (randomized)
        /// You can randomize 3 numbers where each number is tied to one action
        ///     1 = ActionType.Attack
        ///     2 = ActionType.Defend
        ///     3 = ActionType.WildAttack
        /// </summary>
        /// <returns>A randomized action</returns>
        public void SetRandomAction()
        {
            switch (random.Next(1, 3))
            {
                case 1:
                    Action = ActionType.Attack;
                    break;
                case 2:
                    Action = ActionType.Defend;
                    break;
                case 3:
                    Action = ActionType.WildAttack;
                    break;
                default: //fail safe
                    Action = ActionType.Attack;
                    break;
            }
        }

        /// <summary>
        /// Display the name, HP, and stats of this unit (Console.WriteLine)
        /// </summary>
        public void DisplayStats()
        {
            Console.WriteLine(Name + "  HP: " + CurrentHp + "/" + MaxHp + " POW: " + Power + "  DEF: " + Defense);
        }
    } // end class Unit
} // end namespace SuidokenDuel
