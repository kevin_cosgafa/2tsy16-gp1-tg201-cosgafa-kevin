﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Deck
    {
        private Random random = new Random();

        public IEnumerable<Card> CardPile
        {
            get { return cardPile; }
        }
        private Stack<Card> cardPile = new Stack<Card>();
        private List<Card> discardPile = new List<Card>();

        /// <summary>
        /// Deal 4 cards to every participant from the cardPile
        /// </summary>
        /// <param name="participants"></param>
        public void DealCards(ICollection<Participant> participants)
        {
            // Make a new cardPile from CreateDealDeck()
            cardPile.Clear();
            cardPile = new Stack<Card>(CreateDealDeck());
            Shuffle();

            // Deal cards for every participant
            foreach (Participant participant in participants)
            {
                // Deal 4 cards
                for (int i = 0; i < 4; i++) { participant.Hand.Add(DrawCard()); }

                // Add 1 defuse card
                participant.Hand.Add(new Defuse());
            }
        }

        /// <summary>
        /// Completes the deck by adding exploding kittens, defuse cards and shuffling the deck
        /// </summary>
        public void CompleteSetup()
        {
            // Add Exploding Kittens and Defuse cards to complete the deal
            for (int i = 0; i < 4; i++) { cardPile.Push(new ExplodingKitten()); }
            cardPile.Push(new Defuse());

            // Shuffle the cards after adding exploding kittens and defuse cards
            Shuffle();
        }

        public void Shuffle()
        {
            List<Card> tempDeck = new List<Card>(cardPile);
            cardPile.Clear();
            for (int i = 0; i < 50; i++)
            {
                int one = random.Next(tempDeck.Count());
                int two = random.Next(tempDeck.Count());
                if (one == two) { i--; }
                else
                {
                    Card temp = tempDeck[one];
                    tempDeck[one] = tempDeck[two];
                    tempDeck[two] = temp;
                }
            }
            cardPile = new Stack<Card>(tempDeck);
            tempDeck.Clear();
        }

        public Card DrawCard()
        {
            // When the carePile reaches 0 cards, take the discard pile
            // Shuffle it and make it the new cardPile
            if (cardPile.Count <= 0)
            {
                cardPile.Clear();
                cardPile = new Stack<Card>(discardPile);
                Shuffle();

                // Empty the discardPile after making the new cardPile
                discardPile.Clear();
            }
            return cardPile.Pop();
        }

        public bool AddCard(Card card, int index)
        {
            // Returns false when index exceeds the size of cardPile
            if (index >= cardPile.Count()) { return false; }

            List<Card> tempDeck = new List<Card>(cardPile);
            cardPile.Clear();
            tempDeck.Insert(index, card);
            cardPile = new Stack<Card>(tempDeck);
            tempDeck.Clear();
            return true;
        }

        public void AddDiscardedCard(Card card)
        {
            discardPile.Add(card);
        }

        /// <summary>
        /// Returns an array of all playing cards EXCEPT ExplodingKitten and Defuse
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Card> CreateDealDeck()
        {
            // Setup unique card allocation
            Stack<Card> newDeck = new Stack<Card>();

            // 4 for each card
            for (int i = 0; i < 4; i++)
            {
                newDeck.Push(new Attack());
                newDeck.Push(new Favor());
                newDeck.Push(new SeeTheFuture());
                newDeck.Push(new Shuffle());
                newDeck.Push(new Skip());
                newDeck.Push(new CatCard(CatCard.CardType.TacoCat));
                newDeck.Push(new CatCard(CatCard.CardType.RainbowCat));
                newDeck.Push(new CatCard(CatCard.CardType.MelonCat));
                newDeck.Push(new CatCard(CatCard.CardType.BeardCat));
                newDeck.Push(new CatCard(CatCard.CardType.HairyPotatoCat));
            }

            // Additional see future card (according to original game rules, there are 5 see the future cards)
            newDeck.Push(new SeeTheFuture());
            return newDeck;
        }
    }
}
