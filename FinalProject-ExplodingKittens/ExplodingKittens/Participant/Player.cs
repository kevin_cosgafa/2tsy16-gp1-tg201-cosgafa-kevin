﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Player : Participant
    {
        public Player(string name) : base(name)
        {

        }

        public override void EvaluateTurn(ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            // While the Player still has usable cards, ask if the player wants to use a card
            while (!TurnEnded)
            {
                List<Card> choices = new List<Card>();
                foreach (Card card in hand)
                    if (card.CanUseManually) { choices.Add(card); }
                string choice = " ";
                if (choices.Count() > 0)
                {
                    while (true)
                    {
                        Console.WriteLine("Use a card(y/n)?");
                        DisplayHand();
                        choice = Console.ReadLine();
                        if (choice == "y" || choice == "n") { break; }
                    }
                    if (choice == "n") { TurnEnded = true; break; }

                    // Upon using a card, get the card from PlayCard
                    Card discard = PlayCard();

                    // Remove it from the hand
                    hand.Remove(discard);

                    // Use the card
                    discard.Use(this, participants, turnOrder, deck);

                    // Add it to the discard pile of the deck
                    deck.AddDiscardedCard(discard);
                }
                else { break; }
            }
           
            // End of turn, check if I need to draw a card
            if (ShouldDraw) { DrawCard(participants, turnOrder, deck); }

            // Always reset should draw to true, because after every turn you should draw
            ShouldDraw = true;

            // Reset turn ended to false
            TurnEnded = false;
        }

        public override Card PlayCard()
        {
            List<Card> choices = new List<Card>();
            foreach (Card card in hand)
                if (card.CanUseManually) { choices.Add(card); }
            string choice = " ";
            while (true)
            {
                Console.WriteLine("========CARDS========");
                for (int i = 0; i < choices.Count(); i++) { Console.Write((i) + ")"); choices[i].DisplayCard(); }
                choice = Console.ReadLine();
                int index;
                Int32.TryParse(choice, out index);
                if (index >= 0 || index < choices.Count()) { return choices[index]; }
            }
        }
        /// <summary>
        /// Give the user a selection of targets
        /// </summary>
        /// <param name="participants"></param>
        /// <returns></returns>
        public override Participant SelectTarget(ICollection<Participant> participants)
        {
            // Do not select yourself
            List<Participant> targets = new List<Participant>();
            foreach (Participant target in participants)
                if (target != this) { targets.Add(target); }
            
            // Print all participants excluding yourself
            string choice = "-1";
            while (Convert.ToInt32(choice) < 0 || Convert.ToInt32(choice) >= targets.Count)
            {
                for (int i = 0; i < targets.Count(); i++)
                    Console.WriteLine(i + ") " + targets[i].Name);
                choice = Console.ReadLine();
            }
            return targets[Convert.ToInt32(choice)];
        }
    }
}
