﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class AI : Participant
    {
        public AI (string name) : base (name)
        {

        }

        /// <summary>
        /// Play a random card
        /// </summary>
        public override void EvaluateTurn(ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            Console.WriteLine(this.Name + "'s turn");
            // While the AI still has usable cards, do a 50-50 chance to use a card or end the turn
            bool Useable = false;
            foreach (Card card in hand)
                if (card.CanUseManually) { Useable = true; break; }
            while (Useable)
            {
                Console.WriteLine(this.Name + " is thinking...");
                // 50-50 chances to use a card or pass
                if ((float)(random.Next(0, Int32.MaxValue) / Int32.MaxValue) < 0.5f) { break; }
                
                // Upon using a card, get the card from PlayCard
                Card usedCard = PlayCard();
                // Remove it from the hand
                hand.Remove(usedCard);

                // Use the card
                usedCard.Use(this, participants, turnOrder, deck);
                // Add it to the discard pile of the deck
                deck.AddDiscardedCard(usedCard);

                foreach (Card card in hand)
                    if (card.CanUseManually) { Useable = true; break; }
            }       

            // End of turn, check if I need to draw a card
            if (ShouldDraw) {
                DrawCard(participants, turnOrder, deck);
            }

            // Always reset should draw to true, because after every turn you should draw       
            ShouldDraw = true;

            // Reset turn ended to false
            TurnEnded = false;
        }

        public override Card PlayCard()
        {
            List<Card> UseableCards = new List<Card>();
            foreach (Card card in hand)
                if (card.CanUseManually) { UseableCards.Add(card); }
            return UseableCards[random.Next(UseableCards.Count())];
        }

        /// <summary>
        /// Selects a random target that isn't the AI
        /// </summary>
        /// <param name="participants"></param>
        /// <returns></returns>
        public override Participant SelectTarget(ICollection<Participant> participants)
        {
            // Do not select yourself
            Participant target = null;
            List<Participant> targets = new List<Participant>(participants);
            while (true)
            {
                target = targets[random.Next(targets.Count())];
                if (target != this) { return target; }
                target = null;
            }
        }
    }
}
