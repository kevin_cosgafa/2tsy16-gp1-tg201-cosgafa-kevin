﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class CatCard : Card
    {
        public enum CardType
        {
            TacoCat,
            RainbowCat,
            MelonCat,
            BeardCat,
            HairyPotatoCat
        }

        public CardType CatType { get; private set; }

        public CatCard(CardType type)
        {
            CatType = type;
            CanUseManually = true;
            Name = type.ToString();
        }

        public override void DisplayCard()
        {
            Console.WriteLine(Name + " Card");
        }

        /// <summary>
        /// Check if the actor has atleast 2 cat cards
        /// </summary>
        /// <param name="actor"></param>
        /// <param name="participants"></param>
        /// <param name="turnOrder"></param>
        /// <param name="deck"></param>
        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);

            bool canUse = false;
            Card pair = this;
            // Special condition, check if the actor has atleast 1 other cat card of the same ty
            foreach (Card card in actor.Hand)
            {
                if (card.Name == this.Name)
                {
                    canUse = true;
                    pair = card;
                    break;
                }
            }
            // If they can't use this cat card, return the card to the actor and let them select again
            if (!canUse)
            {
                Console.WriteLine(actor.Name + " tried to use " + this.Name + " but doesn't meet the requirements. Returning card...");
                actor.Hand.Add(this);
                return;
            }

            // If they can, grant a Blind Pick
            Participant target = actor.SelectTarget(participants);
            Console.WriteLine(actor.Name + " used a blind steal on " + target.Name + "!");
            Card chosenCard;
            chosenCard = target.Hand.First();
            actor.Hand.Add(chosenCard);

            // Remove one additional cat card w/ the same type after stealing
            actor.Hand.Remove(pair);

            // Remember to add that card to the discard pile
            deck.AddDiscardedCard(pair);
        }
    }
}
