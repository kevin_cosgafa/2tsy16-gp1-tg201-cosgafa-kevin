﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class ExplodingKitten : Card
    {
        public ExplodingKitten()
        {
            CanUseManually = true;
            Name = "Exploding Kitten";
        }

        public override void DisplayCard()
        {
            Console.WriteLine(Name + " Card");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);
            Console.WriteLine(actor.Name + " has drawn an Exploding Kitten!");
            // Actor died, set isAlive to false
            actor.IsAlive = false;

            bool safe = false;
            // Call OnCardUsing on each card in the actor's hand
            foreach (Card card in actor.Hand)
            {
                // If a card has responded, stop the loop and remove that card from the hand
                if (card.OnCardUsing(this, actor, participants, turnOrder, deck) != null)
                {
                    safe = true;
                    actor.Hand.Remove(card);
                    break;
                }
            }
            if (!safe) { Console.WriteLine(actor.Name + " has been blown to smitherines!"); }
        }
    }
}
