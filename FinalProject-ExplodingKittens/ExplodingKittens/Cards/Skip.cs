﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Skip : Card
    {
        public Skip()
        {
            CanUseManually = true;
            Name = "Skip";
        }

        public override void DisplayCard()
        {
            Console.WriteLine(Name + " Card");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);

            Console.WriteLine(actor.Name + " used Skip!");
            // Set the actor's ShouldDraw to false
            actor.ShouldDraw = false;

            // and TurnEnded to true
            actor.TurnEnded = true;
        }
    }
}
