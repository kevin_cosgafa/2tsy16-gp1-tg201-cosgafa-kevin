﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Favor : Card
    {
        public Favor()
        {
            CanUseManually = true;
            Name = "Favor";
        }

        public override void DisplayCard()
        {
            Console.WriteLine(Name + " Card");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);

            // Call SelectTarget from the actor
            Participant target = actor.SelectTarget(participants);
            Console.WriteLine(actor.Name + " has used a blind steal on " + target.Name);
            // Check if that target still has cards
            if (target.Hand.Count() < 0)
            {
                // Randomly steal a card if they still have a card
                List<Card> choices = new List<Card>(target.Hand);
                Card choice = choices[random.Next(choices.Count())];
                target.Hand.Remove(choice);
                actor.Hand.Add(choice);
            }
        }
    }
}
