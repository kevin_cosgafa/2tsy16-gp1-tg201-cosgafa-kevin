﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Defuse : Card
    {
        public Defuse()
        {
            CanUseManually = false;
            Name = "Defuse";
        }

        public override void DisplayCard()
        {
            Console.WriteLine(Name + " Card");
        }

        public override Card OnCardUsing(Card card, Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.OnCardUsing(card, actor, participants, turnOrder, deck);

            // Only react to an ExplodingKitten.
            if (card.Name == "Exploding Kitten")
            {
                // If it's an ExplodingKitten card, reset the actor's isAlive to true
                actor.IsAlive = true;

                // Re-add the ExplodingKitten card to a random place in the deck
                deck.AddCard(card, random.Next(deck.CardPile.Count()));

                // Return this card if you defused an ExplodingKitten card
                Console.WriteLine(actor.Name + " has successfully defused the bomb!");
                return this;
            }
            // Return null if you did not find any ExplodingKitten card
            return null;
        }
    }
}
