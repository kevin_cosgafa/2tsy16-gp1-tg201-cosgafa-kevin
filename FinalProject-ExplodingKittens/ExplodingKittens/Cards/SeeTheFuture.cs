﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class SeeTheFuture : Card
    {
        public SeeTheFuture()
        {
            CanUseManually = true;
            Name = "See The Future";
        }

        public override void DisplayCard()
        {
            Console.WriteLine(Name + " Card");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);

            Console.WriteLine(actor.Name + " has used " + this.Name + " on the deck!");
            // Print the top 3 cards of the deck
            List<Card> list = new List<Card>(deck.CardPile);
            for (int i = 0; i < 3; i++)
                if (i < deck.CardPile.Count()) { Console.WriteLine(list[i].Name); }
        }
    }
}
