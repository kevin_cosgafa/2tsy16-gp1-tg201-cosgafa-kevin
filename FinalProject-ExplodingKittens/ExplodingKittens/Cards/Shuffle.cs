﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Shuffle : Card
    {
        public Shuffle()
        {
            CanUseManually = true;
            Name = "Shuffle";
        }

        public override void DisplayCard()
        {
            Console.WriteLine(Name + " Card");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);

            // Call Shuffle on the deck
            Console.WriteLine(actor + " has shuffled the deck!");
            deck.Shuffle();
        }
    }
}
