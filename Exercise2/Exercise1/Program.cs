﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] items = new string[5] { "Health Potion", "Mana Potion", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe" };
            foreach (string item in items) { Console.WriteLine(item); }

            Console.WriteLine("\nSearch for an item: ");
            string search = Console.ReadLine();

            bool locate = false;
            for (int counter = 0; counter < 5; counter++)
            {
                if (items[counter] == search) { locate = true; }
            }
            if (locate) { Console.WriteLine("Item Found."); }
            else { Console.WriteLine("Item Not Found."); }

            Console.Read();
        }
    }
}