﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise7
{
    class Player : Fighter
    {
        public Player(string Name, int Health, int Mana, Heal HEAL, Attack ATTACK) : base(Name, Health, Mana, HEAL, ATTACK) { }

        public override string GetChoice()
        {
            string choice = "0";
            Console.Write("[1] ");
            this.skills[0].ShowSkill();
            Console.Write("[2] ");
            this.skills[1].ShowSkill();

            choice = Console.ReadLine();
            Console.Clear();
            return choice;
        } // end GetChoice
    } // end Player
} // end Exercise7
