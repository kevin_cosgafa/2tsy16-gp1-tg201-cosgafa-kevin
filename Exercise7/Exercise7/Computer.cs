﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise7
{
    class Computer : Fighter
    {
        public Computer(string Name, int Health, int Mana, Heal HEAL, Attack ATTACK) : base(Name, Health, Mana, HEAL, ATTACK) { }

        private static readonly Random random = new Random();
        public override string GetChoice()
        {
            if (this.mana < this.skills[1].mpCost) { return "1"; } // only attacks if mana is less than heal mana cost

            int choice = random.Next(1, 3);
            if (choice == 2) { choice = random.Next(1, 3); } // increases chance of rolling attack
            return Convert.ToString(choice);
        } // end GetChoice
    } // end Computer : Fighter
} // end Exercise7
