﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise7
{
    class Heal : Skill
    {
        public Heal(string Name, int healPoints, int MPCost) : base(Name, healPoints, MPCost) { }
        public override void ShowSkill() { Console.WriteLine("{0}, heals {1} HP, uses {2} MP", this.name, this.damage, this.mpCost); }
        public override bool Use(Fighter caster, Fighter target)
        {
            if (!base.Use(caster, target))
            {
                Console.WriteLine("Not Enough Mana.");
                return false;
            }

            Console.WriteLine("{0} uses {1}, healing {2} points.", caster.name, this.name, this.damage);
            caster.setHealth(caster.health + this.damage);
            caster.setMana(caster.mana - this.mpCost);
            return true;
        } // end Use
    } // end Heal
} // end Exercise7
