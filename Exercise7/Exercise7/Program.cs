﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise7
{
    class Program
    {
        static void Main()
        {
            // initialized 2 instances of heal and attack for modifiability of OPness of each fighter
            Heal pHeal = new Heal("Heal", 2, 3);
            Attack pAttack = new Attack("Attack", 1, 1);
            Heal eHeal = new Heal("Heal", 2, 3);
            Attack eAttack = new Attack("Attack", 1, 1);

            Player Player = new Player("Player", 10, 0, pHeal, pAttack);
            Computer Enemy = new Computer("AI", 10, 0, eHeal, eAttack);

            while (Player.health > 0 && Enemy.health > 0)
            {
                bool clear = false;
                while (!clear)
                {
                    Player.ShowStats();
                    Enemy.ShowStats();
                    if (Player.MakeMove(Enemy)) { clear = true; } // checks if player made a valid choice
                }
                Enemy.MakeMove(Player);
                Console.WriteLine(); //separates the console output of actions from player UI (GetChoice)
            }

            //endgame scenarios
            if (Player.health > 0 && Enemy.health <= 0) { Console.WriteLine("{0} wins!\n", Player.name); }
            else if (Player.health <= 0 && Enemy.health > 0) { Console.WriteLine("{0} wins!\n", Enemy.name); }
            else { Console.WriteLine("Both sides die. Tie!\n"); }
        } // end Main
    } // end Program
} // end Exercise7
