﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise7
{
    class Attack : Skill
    {
        public Attack(string Name, int Damage, int mpHeal) : base(Name, Damage, mpHeal) { }
        public override void ShowSkill() { Console.WriteLine(" {0}, deals {1} DMG, recovers {2} MP", this.name, this.damage, this.mpCost); }
        public override bool Use(Fighter caster, Fighter target)
        {
            Console.WriteLine("{0} uses {1}, dealing {2} damage.", caster.name, this.name, this.damage);
            target.setHealth(target.health - this.damage);
            caster.setMana(caster.mana + this.mpCost);
            return true;
        } // end Use
    } // end Attack
} // end Exercise7
