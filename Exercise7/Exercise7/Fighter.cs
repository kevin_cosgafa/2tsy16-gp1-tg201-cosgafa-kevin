﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise7
{
    abstract class Fighter
    {
        public string name { get; protected set; }
        public int health { get; protected set; }
        public int mana { get; protected set; }
        protected List<Skill> skills;

        public Fighter(string Name, int Health, int Mana, Heal HEAL, Attack ATTACK)
        {
            skills = new List<Skill>();
            skills.Add(ATTACK);
            skills.Add(HEAL);
            this.name = Name;
            this.health = Health;
            this.mana = Mana;
        }
        public void ShowStats() { Console.WriteLine("{0}\tHP:{1}\tMP:{2}", this.name, this.health, this.mana); }

        public void setHealth(int Health) { this.health = Health; }
        public void setMana(int Mana) { this.mana = Mana; }

        public abstract string GetChoice();
        public bool MakeMove(Fighter target)
        {
            string choice = GetChoice();
            switch (choice)
            {
                case "1":
                    this.skills[0].Use(this, target);
                    return true;
                case "2":
                    if (!this.skills[1].Use(this, target)) { return false; }
                    return true;
                default:
                    Console.WriteLine("{0} is not a valid move.\n", choice);
                    return false;
            } // end switch
        } // end MakeMove
    } // end Fighter
} // end Exercise7