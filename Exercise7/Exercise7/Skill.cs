﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise7
{
    abstract class Skill
    {
        public Skill(string Name, int Damage, int MPCost) {
            this.name = Name;
            this.damage = Damage;
            this.mpCost = MPCost;
        }
        public string name { get; protected set; }
        public int damage { get; protected set; }
        public int mpCost { get; protected set; }
        public virtual bool Use(Fighter caster, Fighter target) { return caster.mana >= this.mpCost; }
        public abstract void ShowSkill();
    } // end Skill
} // end Exercise7
