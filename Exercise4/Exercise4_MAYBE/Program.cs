﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise3
{
    class Program
    {
        static int LinearSearch(string[] items, string itemToSearch)
        {
            int itemsFound = 0;
            foreach (string item in items)
            {
                if (item == itemToSearch) { itemsFound++; }
            }
            return itemsFound;
        }
        static void Main(string[] args)
        {
            string[] items = { "Health Potion", "Mana Potion", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe" };
            foreach (string item in items) { Console.WriteLine(item); }

            Console.Write("\nSearch for an item: ");
            string itemToSearch = Console.ReadLine();
            Console.Write("Found : " + LinearSearch(items, itemToSearch));

            Console.Read();
        }
    }
}
