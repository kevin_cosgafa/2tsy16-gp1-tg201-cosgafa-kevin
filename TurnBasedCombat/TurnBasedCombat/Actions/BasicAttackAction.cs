﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class BasicAttackAction : Action
    {
        // You can use these constants so you don't have to declare them yourself
        protected static readonly float HIT_RATE_MIN = 0.2f;
        protected static readonly float HIT_RATE_MAX = 0.8f;
        protected static readonly float CRIT_MULT = 1.2f;
        protected static readonly float CRIT_CHANCE = 0.2f;

        public BasicAttackAction(Unit actor)
            : base(actor)
        {
        }

        /// <summary>
        /// Apply damage (no skill bonus) to a random opposing unit (team)
        /// Use Combat.TurnOrder to get ALL units
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        public override void Execute(Combat combat)
        {
            // Deducts MP by MpCost. Makes sure that the Action can be executed. Crashes if you call this while not having enough MP. DON'T DELETE THIS.
            base.Execute(combat);
            
            bool select = false;
            while(!select)
            {
                foreach (Unit target in combat.TurnOrder)
                {
                    if (RandomHelper.Chance(0.33f) && target.Team != Actor.Team)
                    {
                        if (WillHit(target))
                        {
                            Console.WriteLine("{0} used {1} on {2}.", Actor.Name, this.Name, target.Name);
                            int damage = Convert.ToInt32((Actor.RandomizeDamage() - target.Stats.Vitality) * Actor.EvaluateDamageMultiplier(target));
                            Console.WriteLine("{0} dealt {1} damage.", Actor.Name, damage);
                            target.CurrentHp -= damage;
                            if (target.CurrentHp == 0) { Console.WriteLine("{0} died.", target.Name); }
                        }
                        else { Console.WriteLine("{0} missed!", Actor.Name); }
                        select = true;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Evaluates if the basic attack should hit or not based on Dex of attacker and Agi of defender
        /// To get hit rate (chance of hitting) use this formula:
        /// hitRate = DEX of attacker / AGI of defender
        /// hitRate cannot go below 20 and cannot go above 80. Clamp the result to this range.
        /// </summary>
        /// <param name="target"></param>
        /// <returns>True if it hits, false otherwise</returns>
        private bool WillHit(Unit target)
        {
            float hitRate = this.Actor.Stats.Dexterity / target.Stats.Agility;
            if (hitRate > HIT_RATE_MAX) { hitRate = HIT_RATE_MAX; }
            if (hitRate < HIT_RATE_MIN) { hitRate = HIT_RATE_MIN; }

            return RandomHelper.Chance(hitRate);
        }

        /// <summary>
        /// Evaluate if there should be a critical bonus
        /// Roll the 20% chance. If it triggers, return crit bonus damage. 
        /// </summary>
        /// <returns>1.2f if crit triggers. 1.0 otherwise</returns>
        private float EvaluateCrit()
        {
            if (RandomHelper.Chance(CRIT_CHANCE)) { return CRIT_MULT; }
            return 1.0f;
        }
    }
}
