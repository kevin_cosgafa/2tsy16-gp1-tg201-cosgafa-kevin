﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class SingleTargetDamageAction : Action
    {
        public SingleTargetDamageAction(Unit unit)
            : base(unit)
        {
        }

        public float DamageMultiplier { get; set; }

        /// <summary>
        /// Apply damage to opposing (team) unit with the lowest HP
        /// Use Combat.TurnOrder to get ALL units
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        public override void Execute(Combat combat)
        {
            // Deducts MP by MpCost. Makes sure that the Action can be executed. Crashes if you call this while not having enough MP. DON'T DELETE THIS.
            base.Execute(combat);

            Unit target = new PlayerUnit(); // initialize dummy target
            Stats stat = new Stats();
            stat.Hp = 1000; // set dummy target max hp extremely high
            target.Stats = stat;
            target.RestoreHpMp(); // set dummy target hp to 100%
            foreach (Unit unit in combat.TurnOrder) // find enemy unit with lowest hp and equate target to unit
                if (unit.Team != Actor.Team && unit.NormalizedHp <= target.NormalizedHp && unit.CurrentHp < unit.Stats.Hp) { target = unit; }
            if (target.CurrentHp == target.Stats.Hp) // if target has full hp, goes for the unit with the lowest max hp
            {
                foreach (Unit unit in combat.TurnOrder)
                    if (unit.Team != Actor.Team && unit.Stats.Hp <= target.Stats.Hp) { target = unit; }
            }

            Console.WriteLine("{0} used {1} on {2}.", Actor.Name, this.Name, target.Name);
            int damage = Convert.ToInt32(((Actor.RandomizeDamage() * DamageMultiplier) - target.Stats.Vitality) * Actor.EvaluateDamageMultiplier(target));
            Console.WriteLine("{0} dealt {1} damage.", Actor.Name, damage);
            target.CurrentHp -= damage;
            if (target.CurrentHp == 0) { Console.WriteLine("{0} died.", target.Name); }
        }
    }
}
