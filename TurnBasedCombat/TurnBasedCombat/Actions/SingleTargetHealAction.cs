﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class SingleTargetHealAction : Action
    {
        public SingleTargetHealAction(Unit actor)
            : base(actor)
        {
        }

        public float HealPercent { get; set; }

        /// <summary>
        /// Heal an ally (same team) with the lowest HP based on set HealPercent
        /// Use Combat.TurnOrder to get ALL units
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        public override void Execute(Combat combat)
        {
            // Deducts MP by MpCost. Makes sure that the Action can be executed. Crashes if you call this while not having enough MP. DON'T DELETE THIS.
            base.Execute(combat);

            Unit target = new PlayerUnit(); // initialize dummy target
            Stats stat = new Stats();
            stat.Hp = 1000; // set dummy target max hp extremely high
            target.Stats = stat;
            target.RestoreHpMp(); // set dummy target hp to 100%
            foreach (Unit unit in combat.TurnOrder) // find friendly unit with lowest hp and equate target to unit
                if (unit.Team == Actor.Team && unit.NormalizedHp < target.NormalizedHp) { target = unit; }
            
            Console.WriteLine("{0} used {1} on {2}.", Actor.Name, this.Name, target.Name);
            int heal = Convert.ToInt32((float)target.Stats.Hp * HealPercent);
            Console.WriteLine("{0} was healed for {1}.", Actor.Name, heal);
            target.CurrentHp += heal;
        }
    }
}
