﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class MultiTargetDamageAction : Action
    {
        public MultiTargetDamageAction(Unit actor)
            : base(actor)
        {
        }

        public float DamageMultiplier { get; set; }

        /// <summary>
        /// Apply damage to ALL opposing units (team). Each damage must be evaluated separately (different damage)
        /// Use Combat.TurnOrder to get ALL units (this includes your ally units as well)
        /// 
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        public override void Execute(Combat combat)
        {
            // Deducts MP by MpCost. Makes sure that the Action can be executed. Crashes if you call this while not having enough MP. DON'T DELETE THIS.
            base.Execute(combat);

            Console.WriteLine("{0} is casting {1}.", Actor.Name, this.Name);
            foreach (Unit target in combat.TurnOrder)
            {
                if (target.Team != Actor.Team)
                {
                    int damage = Convert.ToInt32(((Actor.RandomizeDamage() * DamageMultiplier) - target.Stats.Vitality) * Actor.EvaluateDamageMultiplier(target));
                    Console.WriteLine("{0} dealt {1} damage against {2}", Actor.Name, damage, target.Name);
                    target.CurrentHp -= damage;
                    if (target.CurrentHp == 0) { Console.WriteLine("{0} died.", target.Name); }
                }
            }
        }
    }
}
