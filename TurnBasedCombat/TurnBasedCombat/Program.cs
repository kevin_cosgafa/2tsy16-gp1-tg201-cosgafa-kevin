﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    class Program
    {
        // You don't need to do anything here
        static void Main(string[] args)
        {
            // Prepare team
            Team player = CreatePlayerTeam();
            Team enemy = CreateEnemyTeam();

            // Initiate combat
            Combat combat = new Combat();
            combat.Player = player;
            combat.Enemy = enemy;
            combat.Initiate(); // This will not finish until a winner is determined

            // Combat has ended at this point
            Console.ReadKey();
        }

        /// <summary>
        /// Create a Team of PlayerUnits with all their Actions. Use the functions already available in the supplied classes
        /// To create an Action, use the constructor and pass in the Unit who owns that Action
        /// Remember that you need to instantiate an action for each unit. Do not reuse the same instance for all units.
        /// </summary>
        /// <returns>Player Team</returns>
        static Team CreatePlayerTeam()
        {
            Team team = new Team();
            team.Name = "The Guys";

            PlayerUnit Member1 = NewPlayer("Dude", Job.Warrior, 500, 50, 80, 50, 100, 130);
            BasicAttackAction Action1 = new BasicAttackAction(Member1);
            Action1.Name = "Basic Attack";
            Action1.MpCost = 0;
            MultiTargetDamageAction Action2 = new MultiTargetDamageAction(Member1);
            Action2.Name = "Dude Slam";
            Action2.MpCost = 10;
            Action2.DamageMultiplier = 0.9f;
            team.AddMember(Member1);

            PlayerUnit Member2 = NewPlayer("Bro", Job.Mage, 400, 50, 60, 40, 130, 100);
            BasicAttackAction Action3 = new BasicAttackAction(Member2);
            Action3.Name = "Basic Attack";
            Action3.MpCost = 0;
            SingleTargetHealAction Action4 = new SingleTargetHealAction(Member2);
            Action4.Name = "Bro Heals";
            Action4.MpCost = 10;
            Action4.HealPercent = 0.3f;
            team.AddMember(Member2);

            PlayerUnit Member3 = NewPlayer("Sup", Job.Assassin, 300, 50, 100, 30, 150, 150);
            BasicAttackAction Action5 = new BasicAttackAction(Member3);
            Action5.Name = "Basic Attack";
            Action5.MpCost = 0;
            SingleTargetDamageAction Action6 = new SingleTargetDamageAction(Member3);
            Action6.Name = "Sup Punch";
            Action6.MpCost = 10;
            Action6.DamageMultiplier = 2.2f;
            team.AddMember(Member3);

            return team;
        }

        /// <summary>
        /// Create a Team of EnemyUnits with all their Actions. Use the functions already available in the supplied classes
        /// To create an Action, use the constructor and pass in the Unit who owns that Action.
        /// Remember that you need to instantiate an action for each unit. Do not reuse the same instance for all units.
        /// </summary>
        /// <returns>Enemy Team</returns>
        static Team CreateEnemyTeam()
        {
            Team team = new Team();
            team.Name = "The Fellowship";

            EnemyUnit Member1 = NewEnemy("The Dunadan", Job.Warrior, 500, 50, 80, 50, 100, 130);
            BasicAttackAction Action1 = new BasicAttackAction(Member1);
            Action1.Name = "Anduril";
            Action1.MpCost = 0;
            MultiTargetDamageAction Action2 = new MultiTargetDamageAction(Member1);
            Action2.Name = "The Black Stone";
            Action2.MpCost = 10;
            Action2.DamageMultiplier = 0.9f;
            team.AddMember(Member1);

            EnemyUnit Member2 = NewEnemy("Mithrandir", Job.Mage, 400, 50, 60, 40, 130, 100);
            BasicAttackAction Action3 = new BasicAttackAction(Member2);
            Action3.Name = "Glamdring";
            Action3.MpCost = 0;
            SingleTargetHealAction Action4 = new SingleTargetHealAction(Member2);
            Action4.Name = "Staff of Power";
            Action4.MpCost = 10;
            Action4.HealPercent = 0.3f;
            team.AddMember(Member2);

            EnemyUnit Member3 = NewEnemy("Witch-King of Angmar", Job.Assassin, 300, 50, 100, 30, 150, 150);
            BasicAttackAction Action5 = new BasicAttackAction(Member3);
            Action5.Name = "Sword of the Witch-King";
            Action5.MpCost = 0;
            SingleTargetDamageAction Action6 = new SingleTargetDamageAction(Member3);
            Action6.Name = "Morgul Blade";
            Action6.MpCost = 10;
            Action6.DamageMultiplier = 2.2f;
            team.AddMember(Member3);

            return team;
        }

        // custom funcs
        static PlayerUnit NewPlayer(string name, Job job, int hp, int mp, int pwr, int vit, int agi, int dex)
        {
            PlayerUnit newUnit = new PlayerUnit();
            newUnit.Name = name;
            newUnit.Job = job;
            newUnit.Stats = SetStats(hp, mp, pwr, vit, agi, dex);
            newUnit.RestoreHpMp();
            return newUnit;
        }
        static EnemyUnit NewEnemy(string name, Job job, int hp, int mp, int pwr, int vit, int agi, int dex)
        {
            EnemyUnit newUnit = new EnemyUnit();
            newUnit.Name = name;
            newUnit.Job = job;
            newUnit.Stats = SetStats(hp, mp, pwr, vit, agi, dex);
            newUnit.RestoreHpMp();
            return newUnit;
        }
        static Stats SetStats(int hp, int mp, int pwr, int vit, int agi, int dex)
        {
            Stats stats = new Stats();
            stats.Hp = hp;
            stats.Mp = mp;
            stats.Power = pwr;
            stats.Vitality = vit;
            stats.Agility = agi;
            stats.Dexterity = dex;
            return stats;
        }
    }
}
