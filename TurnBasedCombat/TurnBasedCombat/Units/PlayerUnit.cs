﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class PlayerUnit : Unit
    {
        /// <summary>
        /// Evaluate the action to be used for the PlayerUnit
        /// 1. Display the Unit
        /// 2. Create a list of valid actions (enough MP)
        /// 3. Print the list to display the actions the user can take
        /// 4. Get user input for action selection. Make sure that the user is inputting valid commands
        /// 5. Clear the console
        /// 6. Execute the action (Action.Execute). Remember to pass in the combat parameter to Action.Execute.
        /// </summary>
        /// <param name="combat"></param>
        public override void EvaluateTurn(Combat combat)
        {
            DisplayUnit(); // 1.

            List<Action> choices = new List<Action>(); // 2.

            for (int i = 0; i < Actions.Count(); i++)
            {
                if (Actions[i].CanUse)
                {
                    choices.Add(Actions[i]);
                    Console.WriteLine("\t[{0}] {1} <MP: {2}>", i + 1, Actions[i].Name, Actions[i].MpCost); // 3.
                }

            }

            string choice; // 4.
            while (true)
            {
                choice = Console.ReadLine();
                if (choice == "1" || (choice == "2" && choices.Count >= 2)) { break; }
            }

            Console.Clear(); //5

            choices[Convert.ToInt32(choice) - 1].Execute(combat); //6
        }
    }
}
