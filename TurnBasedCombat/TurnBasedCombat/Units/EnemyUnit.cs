﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class EnemyUnit : Unit
    {
        /// <summary>
        /// Randomize an action to be used by the EnemyUnit
        /// 1. Create a list of valid actions (enough MP)
        /// 2. Get a random Action based on the valid action list
        /// 3. Execute the action (Action.Execute). Remember to pass in the combat parameter to Action.Execute.
        /// </summary>
        /// <param name="combat"></param>
        public override void EvaluateTurn(Combat combat)
        {
            List<Action> choices = new List<Action>(); // 1.
            foreach (Action action in Actions)
                if (action.CanUse) { choices.Add(action); }

            int choice = RandomHelper.Range(choices.Count()); // 2.

            choices[choice].Execute(combat); // 3.
            
        }
    }
}
