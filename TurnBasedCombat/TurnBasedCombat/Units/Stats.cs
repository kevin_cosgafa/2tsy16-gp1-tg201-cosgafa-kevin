﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    /// <summary>
    /// Nothing to do here
    /// </summary>
    public struct Stats
    {
        public int Hp { get; set; }
        public int Mp { get; set; }
        public int Power { get; set; }
        public int Vitality { get; set; }
        public int Agility { get; set; }
        public int Dexterity { get; set; }

        //slightly modified DisplayStats to also show current hp and mp
        public void DisplayStats(int currentHp, int currentMp)
        {
            Console.WriteLine("HP: "+ currentHp + "/" + Hp);
            Console.WriteLine("MP: "+ currentMp + "/" + Mp);
            Console.WriteLine("POW: " + Power);
            Console.WriteLine("VIT: " + Vitality);
            Console.WriteLine("AGI: " + Agility);
            Console.WriteLine("DEX: " + Dexterity);
        }
    }
}
