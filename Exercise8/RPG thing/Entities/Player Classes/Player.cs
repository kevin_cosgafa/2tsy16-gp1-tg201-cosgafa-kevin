﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_thing
{
    public abstract class Player : Entity
    {
        public Player()
        {
            this.position[0] = 0; //x coordinate
            this.position[1] = 0; //y coordinate
            this.exp = 0;
            this.expToLvl = 1000;
            this.level = 1;
        }
        protected string playerName;
        protected int[] position = new int[2];
        protected int expToLvl;
        protected int level;

        public void ShowPos() { Console.WriteLine("{0}, {1}", this.position[0], this.position[1]); }
        public void Move()
        {
            while (true)
            {
                string choice = Console.ReadLine();
                if (choice == "1") { this.position[0]++; break; }
                else if (choice == "2") { this.position[1]++; break; }
                else if (choice == "3") { this.position[0]--; break; }
                else if (choice == "4") { this.position[1]--; break; }
                else { Console.WriteLine("Invalid input!"); }
            }
        }
        public void Rest() { this.health = maxHealth; }
        public void ViewStats()
        {
            Console.WriteLine("{0}\n{1}\n{2}\n{3}/{4}\n{5}/{6}\n{7}\n{8}\n{9}\n{10}/{11}\n\n",
                this.playerName, this.className, this.accuracy, this.health, this.maxHealth,
                this.exp, this.expToLvl, this.level, this.armor, this.weapon.ShowName(),
                this.weapon.ShowDamage()[0], this.weapon.ShowDamage()[1]);
        }
        public void Quit() { System.Environment.Exit(1); }
    }
}
