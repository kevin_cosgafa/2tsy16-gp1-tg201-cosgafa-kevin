﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_thing
{
    public class Fighter : Player
    {
        public Fighter(string PlayerName) : base()
        {
            this.playerName = PlayerName;
            this.className = "Fighter";
            this.accuracy = 10;
            this.maxHealth = 20;
            this.health = this.maxHealth;
            this.armor = 4;
            this.weapon = new Weapon("Long Sword", 1, 8);
        }
    }
}
