﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_thing
{
    public class Thief : Player
    {
        public Thief(string PlayerName) : base()
        {
            this.playerName = PlayerName;
            this.className = "Thief";
            this.accuracy = 7;
            this.maxHealth = 12;
            this.health = maxHealth;
            this.armor = 2;
            this.weapon = new Weapon("Short Sword", 1, 6);
        }
    }
}
