﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_thing
{
    public class Cleric : Player
    {
        public Cleric(string PlayerName) : base()
        {
            this.playerName = PlayerName;
            this.className = "Cleric";
            this.accuracy = 8;
            this.maxHealth = 15;
            this.health = this.maxHealth;
            this.armor = 3;
            this.weapon = new Weapon("Flail", 1, 6);
        }
    }
}
