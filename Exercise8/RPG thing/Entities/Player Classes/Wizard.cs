﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_thing
{
    public class Wizard : Player
    {
        public Wizard(string PlayerName) : base()
        {
            this.playerName = PlayerName;
            this.className = "Wizard";
            this.accuracy = 5;
            this.maxHealth = 10;
            this.health = this.maxHealth;
            this.armor = 1;
            this.weapon = new Weapon("Staff", 1, 4);
        }
    }
}
