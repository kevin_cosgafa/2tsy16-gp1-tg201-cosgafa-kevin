﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_thing
{
    public abstract class Entity
    {
        protected string className;
        protected int accuracy;
        protected int health;
        protected int maxHealth;
        protected int exp;
        protected int armor;
        protected Weapon weapon;
    }
}
