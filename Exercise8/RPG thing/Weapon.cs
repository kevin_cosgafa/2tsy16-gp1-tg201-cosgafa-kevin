﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_thing
{
    public class Weapon
    {
        public Weapon(string Name, int minDamage, int maxDamage)
        {
            this.name = Name;
            this.damageRange[0] = minDamage;
            this.damageRange[1] = maxDamage;
        }
        private string name;
        private int[] damageRange = new int[2];
        public string ShowName() { return name; }
        public int[] ShowDamage() { return this.damageRange; }
    }
}
