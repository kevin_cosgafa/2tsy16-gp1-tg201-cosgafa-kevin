﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise3
{
    class Program
    {
        static bool SearchArray(string[] items, string itemToSearch)
        {
            for (int counter = 0; counter < items.Length; counter++)
            {
                if (items[counter] == itemToSearch) { return true; }
            }
            return false;
        }
        static void Main(string[] args)
        {
            string[] items = { "Health Potion", "Mana Potion", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe" };
            foreach (string item in items) { Console.WriteLine(item); }
            Console.Write("\nSearch for an item: ");
            string itemToSearch = Console.ReadLine();

            if (SearchArray(items, itemToSearch))
                Console.WriteLine("Item Found.");
            else { Console.WriteLine("Item Not Found."); }

            Console.Read();
        }
    }
}
