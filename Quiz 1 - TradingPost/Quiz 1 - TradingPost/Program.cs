﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz_1___TradingPost
{
    class Program
    {
        struct Order
        {
            public string Name;
            public double Price;
        }

        static List<Order> GetDefaultOrders()
        {
            List<Order> defaultOrders = new List<Order>();

            Order o1 = new Order();
            o1.Name = "Rattlesnake";
            o1.Price = 378999988;
            defaultOrders.Add(o1);

            Order o2 = new Order();
            o2.Name = "Rattlesnake";
            o2.Price = 377577659;
            defaultOrders.Add(o2);

            Order o3 = new Order();
            o3.Name = "Rattlesnake";
            o3.Price = 414999900;
            defaultOrders.Add(o3);

            Order o4 = new Order();
            o4.Name = "Rattlesnake";
            o4.Price = 398800000;
            defaultOrders.Add(o4);

            Order o5 = new Order();
            o5.Name = "Drake";
            o5.Price = 48000000;
            defaultOrders.Add(o5);

            Order o6 = new Order();
            o6.Name = "Drake";
            o6.Price = 51975000;
            defaultOrders.Add(o6);

            Order o7 = new Order();
            o7.Name = "Drake";
            o7.Price = 49999999;
            defaultOrders.Add(o7);

            Order o8 = new Order();
            o8.Name = "PLEX";
            o8.Price = 1180000000;
            defaultOrders.Add(o8);

            Order o9 = new Order();
            o9.Name = "PLEX";
            o9.Price = 1177566613;
            defaultOrders.Add(o9);

            Order o10 = new Order();
            o10.Name = "PLEX";
            o10.Price = 1182899876;
            defaultOrders.Add(o10);

            return defaultOrders;
        }

        // Quiz Item
        // Return a list of items that match your item name
        static List<Order> FilterOrders(List<Order> orders, string filterKey)
        {
            List<Order> FilteredItems = new List<Order>();
            for(int i=0; i<orders.Count; i++)
            {
                if (orders[i].Name == filterKey)
                    FilteredItems.Add(orders[i]);
            }
            return FilteredItems;
        }

        // Quiz Item
        // Return a list of ordered items by price
        static List<Order> SortOrders(List<Order> orders, string filterKey)
        {
            if (orders.Count != 0)
            {
                List<Order> SortedItems = orders;
                for (int i = 0; i < SortedItems.Count - 1; i++)
                {
                    for (int j = 0; j < (SortedItems.Count - 1); j++)
                    {
                        if (SortedItems[j].Price > SortedItems[j + 1].Price)
                        {
                            Order temp = SortedItems[j];
                            SortedItems[j] = SortedItems[j + 1];
                            SortedItems[j + 1] = temp;
                        }
                    }
                }
                return SortedItems;
            }
            else
                return orders;
        }

        // Quiz Item
        // Sell the item for a value higher than the price given, or a value equal to the price given
        // If there are no buyers that match your order, print "Your item has been listed"
        static void PostTransaction(List<Order> orders, string name, int price)
        {
            List<Order> items = SortOrders(FilterOrders(orders, name), name);
            if (items.Count != 0 && price <= items[items.Count - 1].Price)
                Console.WriteLine("Your item has ben sold for " + items[items.Count - 1].Price);
            else
                Console.WriteLine("Your item has been listed");
        }

        static void Main(string[] args)
        {
            string item;
            int price;
            List<Order> allOrders = GetDefaultOrders();
            Console.Write("Enter the name of the item you want to sell: ");
            item = Console.ReadLine();

            Console.Write("Enter the amount you want to sell the item for: ");
            price = Convert.ToInt32(Console.ReadLine());

            PostTransaction(allOrders, item, price);

            Console.ReadKey();
        }
    }
}