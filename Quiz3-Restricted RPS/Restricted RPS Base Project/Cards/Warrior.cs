﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Warrior : Card
    {
        //added constructor to automatically declare Type
        public Warrior() { this.Type = "Warrior"; }

        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == this.Type) { return FightResult.Draw; }
            if (opponentCard.Type == "Assassin") { return FightResult.Win; }
            return FightResult.Lose;
        }
    }
}