﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Assassin : Card
    {
        //added constructor to automatically declare Type
        public Assassin() { this.Type = "Assassin"; }

        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == this.Type) { return FightResult.Draw; }
            if (opponentCard.Type == "Mage") { return FightResult.Win; }
            return FightResult.Lose;
        }
    }
}