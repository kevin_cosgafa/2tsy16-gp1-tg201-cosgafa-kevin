﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Mage : Card
    {
        //added constructor to automatically declare Type
        public Mage() { this.Type = "Mage"; }

        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == this.Type) { return FightResult.Draw; }
            if (opponentCard.Type == "Warrior") { return FightResult.Win; }
            return FightResult.Lose;
        }
    }
}