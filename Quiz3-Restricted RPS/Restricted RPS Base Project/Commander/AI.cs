﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            bool WillPlay = RandomHelper.Chance(0.7f);
            if (WillPlay || CanDiscard /* plays card if AI does not have enough cards in hand to discard*/) { this.Fight(opponent); }
            else { this.Discard(); }
        }

        public override Card PlayCard()
        {
            // choose random card in hand to play
            int rand = RandomHelper.Range(this.Cards.Count());
            Card temp = this.Cards[rand];
            this.RemoveCard(this.Cards[rand]);
            return temp;
        }
    }
}
