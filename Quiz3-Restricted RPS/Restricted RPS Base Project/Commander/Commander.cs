﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public abstract class Commander
    {
        public string Name { get; set; }
        public int Points { get; protected set; }

        private List<Card> cards = new List<Card>();
        public IList<Card> Cards { get { return cards; } }
        public bool EmptyHand { get { return cards.Count == 0; } }

        /// <summary>
        /// Can only discard if commander has at least 2 cards.
        /// </summary>
        public bool CanDiscard
        {
            get
            {
                if (this.cards.Count >= 2) { return true; }
                return false;
            }
        }

        /// <summary>
        /// Draw a random card and add it to your hand.
        /// </summary>
        /// <returns></returns>
        public Card Draw()
        {
            // copied and slightly modified the code from program.cs void AddStartingCards
            int rand = RandomHelper.Range(3);
            Card drawnCard;
            switch (rand)
            {
                case 0:
                    drawnCard = new Warrior();
                    break;
                case 1:
                    drawnCard = new Mage();
                    break;
                case 2:
                    drawnCard = new Assassin();
                    break;
                default:
                    drawnCard = new Warrior();
                    break;
            }
            this.cards.Add(drawnCard);
            return drawnCard;
        }

        /// <summary>
        /// Discards two random cards in exchange of one random card. Cannot discard if player has only one card (throws an exception)
        /// </summary>
        /// <returns>Received card after discarding.</returns>
        public Card Discard()
        {
            if (!CanDiscard) throw new Exception("Cannot discard now."); // DON'T DELETE THIS LINE

            Console.WriteLine("\n{0} sacrificed 2 units to summon a new hero", this.Name);
            for (int i = 0; i < 2; i++)
            {
                int rand = RandomHelper.Range(this.cards.Count);
                this.cards.Remove(this.cards[rand]);
            }
            return this.Draw();
        }

        /// <summary>
        /// Display this commander's cards. NOTE: Only call this for the player's turn.
        /// </summary>
        public void DisplayCards()
        {
            Console.WriteLine("Hand");
            foreach (Card card in this.cards) { Console.WriteLine("\t{0}", card.Type); }
            Console.WriteLine();
        }

        /// <summary>
        /// Called whenever one side's hand is empty. All cards will be discarded. Each discarded card will reduce the player's point by 1.
        /// </summary>
        public void OnGameEnding()
        {
            foreach (Card card in this.cards) { this.Points--; }
            if (Points < 0) { Points = 0; }
            this.cards.Clear();
        }

        /// <summary>
        /// Commander's action upon his turn. Can either "Play" or "Discard"
        /// </summary>
        /// <param name="opponent"></param>
        public abstract void EvaluateTurn(Commander opponent);

        /// <summary>
        /// Choose a card to play. Card must be discarded after playing.
        /// </summary>
        /// <returns>Card to play.</returns>
        public abstract Card PlayCard();

        /// <summary>
        /// Each commander plays a card. Points are evaluated here.
        /// Free code :)
        /// </summary>
        /// <param name="opponent"></param>
        public void Fight(Commander opponent)
        {
            //added console output for challenging action
            Console.WriteLine("\n{0} challenged {1} to a fight...\n", this.Name, opponent.Name);

            Card myCard = PlayCard();
            Card opponentCard = opponent.PlayCard();

            //added console output for played cards
            Console.WriteLine("\n{0} summons {1}\n{2} summons {3}\n", this.Name, myCard.Type, opponent.Name, opponentCard.Type);

            FightResult result = myCard.Fight(opponentCard);
            if (result == FightResult.Win)
            {
                Points += 2;

                //added console output for fight evaluation
                Console.WriteLine("{0} won the round", this.Name);
            }
            else if (result == FightResult.Lose) { Console.WriteLine("{0} lost the round", this.Name); }
            else if (result == FightResult.Draw) { Console.WriteLine("Draw!"); }
        }

        //added custom function for removing cards in List<Card> cards
        public void RemoveCard(Card card) { this.cards.Remove(card); }
    }
}
