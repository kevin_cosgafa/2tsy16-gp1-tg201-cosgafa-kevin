﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            if (this.CanDiscard) // will force player to play last card
            {
                while (true) // for invalid inputs
                {
                    this.DisplayCards();
                    Console.WriteLine("\nPress 1 to Challenge {0}\nPress 2 to Discard", opponent.Name);
                    ConsoleKey input = Console.ReadKey().Key;
                    if (input == ConsoleKey.D1) { this.Fight(opponent); break; }
                    else if (input == ConsoleKey.D2)
                    {
                        Card temp = this.Discard();
                        Console.WriteLine("You've drawn {0}", temp.Type);
                        break;
                    }
                    Console.WriteLine("\nInvalid input!");
                } // end while
            } // end if
            else { this.Fight(opponent); }
        } // end EvaluateTurn

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            List<Card> options = new List<Card>();
            int choice;

            // code for options
            bool war = false, mag = false, ass = false;
            foreach (Card card in Cards)
            {
                if (card.Type == "Warrior" && !war) { options.Add(new Warrior()); war = true; }
                else if (card.Type == "Mage" && !mag) { options.Add(new Mage()); mag = true; }
                else if (card.Type == "Assassin" && !ass) { options.Add(new Assassin()); ass = true; }
            }

            this.DisplayCards();
            while(true)
            {
                Console.WriteLine("\nSelect a card to play...");
                // shows options
                for(int i = 0; i < options.Count; i++) { Console.WriteLine("[{0}] {1}", i + 1, options[i].Type); }

                ConsoleKey input = Console.ReadKey().Key;
                Console.WriteLine();
                if (input == ConsoleKey.D1) { choice = 0; break; }
                else if (input == ConsoleKey.D2 && options.Count >= 2) { choice = 1; break; }
                else if (input == ConsoleKey.D3 && options.Count == 3) { choice = 2; break; }

                Console.WriteLine("\nInvalid input!");
            }

            for(int i = 0; i < Cards.Count; i++)
            {
                if (Cards[i].Type == options[choice].Type) { choice = i; break; }
            }
            Card temp = Cards[choice];
            this.RemoveCard(Cards[choice]);
            return temp;
        }
    }
}
